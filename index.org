#+TITLE: Learning the C programming language

The goal of this project, is to learn the C programming language; after all, most languages I will write in are based on C. I will start by learning from [[https://youtu.be/KJgsSFOSQv0][this youtube series]], then I will work through a bunch of the Exercism challenges. This repo will contain most of my notes of the video, along with some random scratch work of the language. Hopefully this is a productive use of my time.


* Hello, World!

** The GNU C Compiler
I will be compiling my programs using =gcc=, the GNU C Compiler (or the GNU Compiler Collection??? idk). Single letter flags aren't grouped (i.e. =-dv= is not the same as =-d -v=. The most useful flags for =gcc= are:
+ =-o=: specify the output file
+ =-g=: produces debugging information
+ =-l= and =-L=: link to a shared library
  - =-l= is name of shared library
  - =-L= is location of shared library
+ =-f[OPTION]= useful for many options, such as including OpenMP with =-fopenmp=
+ =-Werror=: convert warnings to errors, so you actually fix them
+ =-Wall=: enables /all/ warnings
You can also save the flags in a text file called =flags= and then use the =@flags= option with =GCC= to include them as if they were typed. Common use of =GCC= and then running the output file will look like:

#+begin_src bash
$ gcc program.c -o program.o

$ ./program.o
#+end_src

** My first C program
Let's write an annotated hello world:

#+begin_src c
// include standard input and output in a header file
#include <stdio.h>

// include the C standard library
#include <stdlib.h>

// the main function returns 0 on success (i hope to learn more about this later)
int main() {
  printf("Hello, World!\n");
  return 0;
}
#+end_src

* The Basics of C

+ The =main= method is what is executed when the program is
+ Translate C code into machine code
+ Then, run the built code
+ Program is set of instructions
+ =;= ends C instruction

** Drawing shapes with =printf=

#+begin_src c
int main() {
  printf("    /|\n");
  printf("   / |\n");
  printf("  /  |\n");
  printf(" /   |\n");
  printf("/____|\n");

  return 0;
}
#+end_src

** Variables
+ track data and information
+ store data values

*** Variable Declaration
+ declare which type of data to store
  - =char= is used to store characters
  - use =[]= to store multiple characters
  - =int= is signed 8 byte integer

#+begin_src c
char characterName[] = "Timmy";
int characterAge = 20;
#+end_src

*** =printf= is cool
+ ="%s"= is used to insert string
+ ="%d"= is used to insert integer

#+begin_src c
printf("My name is %s\n", characterName);
printf("I am %d years old\n", characterAge);
#+end_src

** Data Types
+ =int=: signed 8 byte integers
+ =double=: floating/decimal point number
  - =40.0= isn't the same as =40=
+ =char=: a single character
  - use single quotes
+ =char []=: a string of characters
  - effectively an array of chars
  - double quotes
  - can't modify the same was as others

*** Examples

#+begin_src c
int age = 40;
double gpa = 3.9;
char grade = 'A';
char string[] = "This is a string :)";
#+end_src

* Slightly Less Basic

** =printf= function
+ performs task
+ formats thing and prints to screen
+ special characters
  - =\= escape character works as in other languages
+ format specifier to print things that aren't plain text
  - ="%d"= for integers
  - ="%s"= for string
  - ="%f"= for floating point number
  - ="%c"= for character

** Working with Numbers
+ common math operators
  - =+=
  - =-=
  - =*=
  - =/=
  - =%=
+ ints are converted to floats if used in operation with them
+ division between ints does floor (like in other langs)
+ more complex functions
  - =pow(2, 3)=
    - returns 8.0
  - =sqrt(36)=
    - returns 6.0
    - good practice to include math header for many of these complex functions
  - =ceil()=
  - =floor()=

** Comments
+ ignored by compiler
+ =/* comment here */=
  - good for multiline
+ =// comment here=
  - single line

** Constants
+ can't be modified
+ =const= keyword
+ convention is upper case (underscores)

#+begin_src c
const int NUM = 1337;
#+end_src

* Building Interactive Programs

** Getting User Input
+ use =printf= for prompt
+ =scanf= to enter information

#+begin_src c
int age;
printf("enter your age: ");
scanf("%d", &age);
printf("you are %d years old...\n", age);
#+end_src

+ ="%lf"= for floating point numbers
+ strings are a bit different:

#+begin_src c
char name[20];
printf("enter your name: ");
scanf("%s", name);
printf("your name is %s\n", name);
#+end_src

+ =scanf= splits on spaces for string input
+ =fgets= gets an entire line of text from user
  - string only
  - first parameter is variable to store input to
  - second parameter is number of characters user can input
  - third parameter is where to get input (i.e. =stdin=)
  - newline is included at end of string

** Example: A Basic Calculator

#+begin_src c
double num0, num1;
printf("enter first number: ");
scanf("%lf", &num0);
printf("enter second number: ");
scanf("%lf", &num1);
printf("the sum: %f\n", num0 + num1);
#+end_src

** Example: Madlibs

#+begin_src c
char color[16], pluralNoun[16], celebrityF[16], celebrityL[16];

printf("enter a color: ");
scanf("%s", color);
printf("enter a plural noun: ");
scanf("%s", pluralNoun);
printf("enter a celebrity name: ");
// this handles two names
// it will not work with only one name though
scanf("%s%s", celebrityF, celebrityL);

printf("Roses are %s\n", color);
printf("%s are blue\n", pluralNoun);
printf("I love %s %s\n", celebrityF, celebrityL);
#+end_src

* Data Structures, Functions, Conditionals, and more!

** Arrays
+ used for large amounts of related information (i.e. lists)
+ data structure to store many data values
*** Initilization of Arrays:
#+begin_src c
int numbers[] = {1, 3, 3, 6,};
printf("%d\n", numbers[0]); // prints 1

printf("%d\n", numbers[3]); // prints 6
numbers[3] = 7;
printf("%d\n", numbers[3]); // prints 7
#+end_src

If the array isn't declared upon initialization, need to declare the size (for memory allocation):

#+begin_src c
int numbers[10];

numbers[0] = 42;
numbers[9] = 1337;

printf("%d\n", numbers[5]); // prints 0
#+end_src

*** So what are strings, anyways?
+ strings are just arrays of characters!
+ double quote assignment is used just because of how common strings are

** Functions
+ collection of code which performs specific task
+ =main= function gets executed when running program
+ declaring function looks like =RETURN_TYPE FUNCTION_NAME(PARAM0, PARAM1, ...)=
  - =void= is used for no return
+ function has to be called (i.e. from =main=)

*** Example
#+begin_src c
int main() {
  sayHelloToTheWorld();
  return 0;
}


void sayHelloToTheWorld() {
  printf("Hello, World!\n");
}
#+end_src

*** Passing in Parameters
+ specify type and name of parameter

#+begin_src c
void sayHelloToTheUser(char name[], int age) {
  printf("Hello, %s\n", name);
  printf("You are %d years old\n", age);
}
#+end_src

*** Returning from Functions
+ return information from function to where they were called
+ specify return type when declaring function
+ value /must/ be returned if not declared =void=
+ want to define function /before/ calling it in =main=
+ breaks out of function

#+begin_src c
double cube(double n) {
  return n * n * n;
}
#+end_src

*** Prototyping Functions
+ write out function signature
  - ~double cube(double n);~
+ makes it so the =cube= function can be declared after the =main= function
+ this can also be done in header files I think
  - maybe this is best practice?

** If Statements
+ act based on circumstances

#+begin_src c
int max(int num0, int num1) {
  int result;
  if (num0 > num1) {
    result = num0;
  }
  else {
    result = num1;
  }
  return result;
}
#+end_src

+ =&&=, =||=, =!=, is all as expected

#+begin_src c
int max(int num0, int num1, int num2) {
  if (num0 >= num1 && num0 >= num2) {
    return num0;
  }
  else if (num1 >= num0 && num1 >= num2) {
    return num1;
  }
  else {
    return num2;
  }
}
#+end_src

+ can negate boolean operation by surrounding with parens and preceding with =!=

** Example: Building a Better Calculator

#+begin_src c
#include <stdio.h>
#include <stdlib.h>


int main() {
  double num0, num1;
  char op;


  printf("enter first number: ");
  scanf("%lf", &num0);

  printf("enter second number: ");
  scanf("%lf", &num1);

  printf("enter operator: ");
  scanf(" %c", &op);  // need space before %c

  if (op == '+') {
    printf("%f %c %f = %f\n", num0, op, num1, num0 + num1);
  }
  else if (op == '-') {
    printf("%f %c %f = %f\n", num0, op, num1, num0 - num1);
  }
  else if (op == '*') {
    printf("%f %c %f = %f\n", num0, op, num1, num0 * num1);
  }
  else if (op == '/') {
    printf("%f %c %f = %f\n", num0, op, num1, num0 / num1);
  }
  else {
    printf("something went wrong...\n");
    return 1;
  }

  return 0;
}
#+end_src

** Switch Statements
+ if statement but comparing one value to a bunch of other values
+ need break statements to not do the other statements

#+begin_src c
char grade = 'F';

switch (grade) {
case 'A' :
  printf("great job\n");
  break;
case 'B' :
  printf("good job\n");
  break;
case 'C' :
  printf("ok job\n");
  break;
case 'D' :
  printf("bad job\n");
  break;
case 'F' :
  printf("terrible job\n");
  break;
default :
  printf("invalid grade");
}
#+end_src

** Structs
+ data structure used to store groups of data types
+ can be used to model things IRL
  - maybe the closest C has to OOP
+ convention to capitalize the Name
+ like a container for data
+ can't assign directly to a char array (string) in C
  - use =strcpy= instead
+ accessed through =.=
  - =student0.name= gives the name of the student

#+begin_src c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct Student {
  char name[16];
  char major[16];
  int age;
  double gpa;
};


int main() {
  // creates a conatainer for a student
  struct Student student0;
  student0.age = 20;
  student0.gpa = 3.9;
  // can't assign directly to char array in C
  strcpy(student0.name, "popska");
  strcpy(student0.major, "computer science");

  printf("name: %s\n", student0.name);
  printf("gpa: %f\n", student0.gpa);
  printf("age: %d\n", student0.age); // for some reason, this prints out 0 unless the age assignment is moved until after the name assignment

  return 0;
}
#+end_src

** While Loops
+ loop over code until condition is false
+ avoid infinite loops

#+begin_src c
int i = 0;
while (i < 5) {
  printf("%d\n", i);
  i++;
}
#+end_src

*** do while loop

#+begin_src c
int i = 6;
do {
  printf("%d\n", i);
} while (i <= 5);
#+end_src

** Example: Guessing Game

#+begin_src c
int guess, i = 0, secretNum = 5;

while (guess != secretNum && i < 3) {
  printf("enter input number: ");
  scanf("%d", &guess);
  i++;
}

printf("correct!\n");
#+end_src

** For Loops
+ uses indexing variable

#+begin_src c
for (int i = 0; i < 5; i++) {
  printf("%d\n", i);
}
#+end_src

** Multidimensional Arrays and Nested Loops
*** 2d Arrays
+ declare with two pairs of square brackets
  - =int nums[][];=
+ access with multiple brackets
  - =nums[2][4]=
+ don't have to give initial values of course

#+begin_src c
int nums[3][2] = {
  {1, 2,},
  {3, 4,},
  {5, 6,},
};

printf("%d\n", nums[0][1]); // prints 2
#+end_src

*** Nested For Loops

#+begin_src c
int i, j;

for (i = 0; i < 3; i++) {
  for (j = 0; j < 2; j++) {
    printf("%d,", nums[i][j]);
  }
  printf("\n");
}
#+end_src

* Managing My Own Memory

** Memory Addresses
+ things in variables are stored in specific memory location
+ C has to use address to access variable
+ can print out the actual address of a variable using:
  - =printf("%p", &var);=
  - note the ampersand
+ this gives the hex value of the address

** Pointers
+ type of data which can be used
+ a memory address
+ =&= gives the physical address in memory
  - =&var= is a /pointer/
+ can create a pointer variable
  - the memory needs a memory address
  - store memory address of *another variable* in the program

#+begin_src c
int age = 20;
int * pAge = &age;
double gpa = 3.4;
double * pGpa = &gpa;
char grade = 'A';
char * pGrade = &grade;
#+end_src

** Dereferencing Pointers
+ gets value at the pointer memory address
+ =*= is used to dereference pointer

#+begin_src c
int age = 20;
int *pAge = &age;
printf("memory address: %p\nwhat's at the memory address: %d", pAge, *pAge);
#+end_src

*** =&= and =*=
+ =&= goes from /value/ to /address (pointer)/
+ =*= goes from /address (pointer)/ to /value/
+ can use =&= and =*= together if you're feeling crazy

* File I/O

** Writing to Files
+ ~FILE * fpointer = fopen("nameOfFile.txt", 'w');~
  - second param is mode:
    - ='r'= for read
    - ='w'= for write
    - ='a'= for append
+ make sure to call ~fclose(fpointer);~ once done with the file
+ =fprintf()= to write to file

#+begin_src c
FILE * fpointer = fopen("example.txt", "w");

fprintf(fpointer, "Timmy: Sales\nRosie: Dog\n");

fclose(fpointer);
#+end_src

+ write mode overwrites everything in the file
+ append mode allows adding to end of file

** Reading from Files
+ open file in read mode
  - ~FILE * fpointer = fopen("example.txt", "r");~
+ create a string variable to store the lines
  - ~char line[255]~
+ use =fgets(line, maxSize, fpointer);=
  - this moves the pointer to the next line

#+begin_src c
char line[255];
FILE * fpointer = fopen("example.txt", "r");

fgets(line, 255, fpointer);
printf("%s", line);

fclose(fpointer);
#+end_src
